# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :sms_browser,
  ecto_repos: [SmsBrowser.Repo]

# Configures the endpoint
config :sms_browser, SmsBrowserWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "sjL3fYC5+bT/sCxynL9ivFBU30bkG90txlK5+GIZtfejH2RZGNIULuZ7i73mnsaa",
  render_errors: [view: SmsBrowserWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SmsBrowser.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
