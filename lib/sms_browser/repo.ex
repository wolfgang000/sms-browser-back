defmodule SmsBrowser.Repo do
  use Ecto.Repo,
    otp_app: :sms_browser,
    adapter: Ecto.Adapters.Postgres
end
