defmodule SmsBrowserWeb.PageController do
  use SmsBrowserWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
